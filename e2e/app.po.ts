import { browser, by, element, ExpectedConditions as EC } from 'protractor';

export class AppPage {
  navigateTo() {
    return browser.get('/');
  }

  getLogoText() {
    return element(by.css('.app-header-logo a')).getText();
  }

  getSearchForm() {
    return element(by.css('app-search-input'));
  }

  getLoginMenuButton() {
    return element(by.css('.app-header-menu a.login-btn'));
  }

  getLogoutMenuButton() {
    browser.sleep(500);
    return element(by.css('.app-header-menu a.logout-btn'));
  }

  clickLoginButton() {
    const loginButton = element(by.css('.login-page button.button'));
    browser.wait(EC.elementToBeClickable(loginButton), 5000);
    return loginButton.click();
  }
}
