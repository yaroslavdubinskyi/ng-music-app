import { AppPage } from './app.po';

describe('ng-music-app App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  describe('{NO_AUTH}', () => {
    describe('[app-header]', () => {
      it('should display app logo', () => {
        page.navigateTo();
        expect(page.getLogoText()).toEqual('NG-MUSIC');
      });
      it('should display login menu button', () => {
        page.navigateTo();
        expect(page.getLoginMenuButton().isEnabled()).toBeTruthy();
      });
    });
  });
  describe('{WITH_AUTH}', () => {
    describe('[app-header]', () => {
      it('should display logout menu button', () => {
        page.navigateTo();
        page.clickLoginButton();
        expect(page.getLogoutMenuButton().isEnabled()).toBeTruthy();
      });
    });
    describe('[search-component]', () => {
      it('should render search form if logged in', () => {
        page.navigateTo();
        expect(page.getSearchForm().isEnabled()).toBeTruthy();
      });
    });
  });
});
