import { browser, by, element, ExpectedConditions as EC } from 'protractor';

export class ContactFormPage {
  navigateTo() {
    return browser.get('/contact');
  }

  getPageTitleText() {
    return element(by.css('app-contact-form-page .section-title')).getText();
  }

  getContactForm() {
    return element(by.css('app-contact-form-page .contact-form'));
  }

  getSubmitButton() {
    return element(by.css('app-contact-form-page .contact-form .button'));
  }

  getFirstNameField() {
    return element(by.id('firstName'));
  }

  getSecondNameField() {
    return element(by.id('secondName'));
  }

  getEmailField() {
    return element(by.id('email'));
  }

  getPhoneField() {
    return element(by.id('phone'));
  }

  getSubjectField() {
    return element(by.id('subject'));
  }

  getMessageField() {
    return element(by.id('message'));
  }
}
