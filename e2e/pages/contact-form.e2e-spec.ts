import { ContactFormPage } from './contact-form.po';

describe('Contact form page', () => {
  let page: ContactFormPage;

  beforeEach(() => {
    page = new ContactFormPage();
  });

  it('should display page title', () => {
    page.navigateTo();
    expect(page.getPageTitleText()).toEqual('Contact form');
  });

  describe('[contact form]', () => {
    it('should render contact form', () => {
      page.navigateTo();
      expect(page.getContactForm().isEnabled()).toBeTruthy();
    });

    it('should display form submit button disabled on start', () => {
      page.navigateTo();
      expect(page.getSubmitButton().isEnabled()).toBeFalsy();
    });

    it('should display form submit button enabled after entering data to form', () => {
      page.navigateTo();
      page.getFirstNameField().sendKeys('Test');
      page.getSecondNameField().sendKeys('Test');
      page.getEmailField().sendKeys('test@gmail.com');
      page.getPhoneField().sendKeys('123456789');
      page.getSubjectField().sendKeys('Test');
      page.getMessageField().sendKeys('Test long sentence lorem ipsum.');

      expect(page.getSubmitButton().isEnabled()).toBeTruthy();
    });

    it('should display form submit button disabled on invalid email input', () => {
      page.navigateTo();
      page.getFirstNameField().sendKeys('Test');
      page.getSecondNameField().sendKeys('Test');
      page.getEmailField().sendKeys('test');
      page.getPhoneField().sendKeys('123456789');
      page.getSubjectField().sendKeys('Test');
      page.getMessageField().sendKeys('Test long sentence lorem ipsum.');

      expect(page.getSubmitButton().isEnabled()).toBeFalsy();
    });

    it('should display form submit button disabled on invalid message input', () => {
      page.navigateTo();
      page.getFirstNameField().sendKeys('Test');
      page.getSecondNameField().sendKeys('Test');
      page.getEmailField().sendKeys('test@gmail.com');
      page.getPhoneField().sendKeys('123456789');
      page.getSubjectField().sendKeys('Test');
      page.getMessageField().sendKeys('Test');

      expect(page.getSubmitButton().isEnabled()).toBeFalsy();
    });
  });
});
