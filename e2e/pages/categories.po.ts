import { browser, by, element, ExpectedConditions as EC } from 'protractor';

export class CategoriesPage {
  navigateTo() {
    return browser.get('/categories');
  }

  getPageTitleText() {
    return element(by.css('app-categories-page .section-title')).getText();
  }

  getCategoriesItems() {
    const item = element(by.css('app-categories-page app-categories-item'));
    browser.wait(EC.elementToBeClickable(item), 5000);
    return item;
  }

  clickCategoriesItem() {
    const item = element(by.css('app-categories-page app-categories-item'));
    browser.wait(EC.elementToBeClickable(item), 5000);
    return item.click();
  }
}
