import { browser } from 'protractor';
import { CategoriesPage } from './categories.po';

describe('Categories page', () => {
  let page: CategoriesPage;

  beforeEach(() => {
    page = new CategoriesPage();
  });

  it('should display page title', () => {
    page.navigateTo();
    expect(page.getPageTitleText()).toEqual('Categories & Moods');
  });

  it('should display categories items', () => {
    page.navigateTo();
    expect(page.getCategoriesItems().isDisplayed()).toBeTruthy();
  });

  it('should redirect to single category page on category item click', () => {
    page.navigateTo();
    page.clickCategoriesItem();
    expect(browser.getCurrentUrl()).toMatch('/categories/test');
  });
});
