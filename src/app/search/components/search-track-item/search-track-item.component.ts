import { Component, Input } from '@angular/core';
import { trigger, state, style, animate, transition } from '@angular/animations';

import { Track } from '../../../models/track.model';

@Component({
  selector: 'app-search-track-item',
  templateUrl: './search-track-item.component.html',
  styleUrls: ['./search-track-item.component.scss'],
  animations: [
    trigger('slideInOut', [
      state('in', style({ transform: 'translateY(0)', opacity: 1 })),
      transition('void => *', [style({ transform: 'translateY(50px)', opacity: 0 }), animate(150)]),
      transition('* => void', [animate(150, style({ transform: 'translateY(0)', opacity: 0 }))]),
    ]),
  ],
})
export class SearchTrackItemComponent {
  @Input() details: Track;

  get name() {
    return this.details.name;
  }

  get artists() {
    return this.details.artists.map(item => item.name).join(', ');
  }

  get album() {
    return this.details.album.name;
  }

  get trackURL() {
    return this.details.external_urls.spotify;
  }
}
