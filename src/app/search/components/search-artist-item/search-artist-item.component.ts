import { Component, Input } from '@angular/core';
import { trigger, state, style, animate, transition } from '@angular/animations';

import { ArtistFull } from '../../../models/artist.model';

@Component({
  selector: 'app-search-artist-item',
  templateUrl: './search-artist-item.component.html',
  styleUrls: ['./search-artist-item.component.scss'],
  animations: [
    trigger('slideInOut', [
      state('in', style({ transform: 'translateY(0)', opacity: 1 })),
      transition('void => *', [style({ transform: 'translateY(50px)', opacity: 0 }), animate(150)]),
      transition('* => void', [animate(150, style({ transform: 'translateY(0)', opacity: 0 }))]),
    ]),
  ],
})
export class SearchArtistItemComponent {
  @Input() details: ArtistFull;

  get name() {
    return this.details.name;
  }

  get genres() {
    return this.details.genres.join(', ');
  }

  get imgSrc() {
    return this.details.images[0].url;
  }

  get artistURL() {
    return this.details.external_urls.spotify;
  }
}
