import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { StoreModule, Store, combineReducers } from '@ngrx/store';

import { SearchFormComponent } from './search-form.component';

import * as fromSearch from '../../reducers';
import * as fromAuth from '../../../auth/reducers';
import { SearchInputComponent } from '../../components/search-input/search-input.component';
import { SearchTrackItemComponent } from '../../components/search-track-item/search-track-item.component';
import { SearchArtistItemComponent } from '../../components/search-artist-item/search-artist-item.component';

describe('SearchFormComponent', () => {
  let component: SearchFormComponent;
  let fixture: ComponentFixture<SearchFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        StoreModule.forRoot({
          search: combineReducers(fromSearch.reducer),
          auth: combineReducers(fromAuth.reducers)
        }),
      ],
      declarations: [ SearchFormComponent, SearchInputComponent, SearchArtistItemComponent, SearchTrackItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
