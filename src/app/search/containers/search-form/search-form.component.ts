import { Component, OnInit } from '@angular/core';
import {
  trigger,
  style,
  transition,
  animate,
  keyframes,
  query,
  stagger,
} from '@angular/animations';
import { ActivatedRoute } from '@angular/router';
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';

import * as fromSearch from '../../reducers';
import * as fromAuth from '../../../auth/reducers';
import * as search from '../../actions/search.actions';

import { Track } from '../../../models/track.model';
import { Artist } from '../../../models/artist.model';

@Component({
  selector: 'app-search-form',
  templateUrl: './search-form.component.html',
  animations: [
    trigger('gridAnimation', [
      transition('* => *', [
        query(':enter', style({ opacity: 0 }), { optional: true }),
        query(
          ':enter',
          stagger('10ms', [
            animate(
              '200ms ease-in',
              keyframes([
                style({ opacity: 0, transform: 'translateY(50px)', offset: 0 }),
                style({ opacity: 1, transform: 'translateY(0)', offset: 1.0 }),
              ]),
            ),
          ]),
          { optional: true },
        ),
        query(
          ':leave',
          stagger('10ms', [
            animate(
              '1s ease-in',
              keyframes([style({ opacity: 1, offset: 0 }), style({ opacity: 0, offset: 1.0 })]),
            ),
          ]),
          { optional: true },
        ),
      ]),
    ]),
  ],
})
export class SearchFormComponent {
  loggedIn$: Observable<boolean>;
  loaded$: Observable<boolean>;
  searchTracks$: Observable<Track[]>;
  searchArtists$: Observable<Artist[]>;
  searchQuery$: Observable<string>;

  constructor(private store: Store<fromSearch.SearchState>) {
    this.loaded$ = store.pipe(select(fromSearch.getSearchLoaded));
    this.searchTracks$ = store.pipe(select(fromSearch.getSearchResultsTracks));
    this.searchArtists$ = store.pipe(select(fromSearch.getSearchResultsArtists));
    this.searchQuery$ = store.pipe(select(fromSearch.getSearchQuery));
    this.loggedIn$ = this.store.pipe(select(fromAuth.getLoggedIn));
  }

  search(querySearch: string) {
    this.store.dispatch(new search.SearchItem(querySearch));
  }
}
