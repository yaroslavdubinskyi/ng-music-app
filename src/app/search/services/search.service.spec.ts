import { TestBed, inject } from '@angular/core/testing';
import { HttpClientModule } from '@angular/common/http';
import { StoreModule, Store, combineReducers } from '@ngrx/store';

import { SearchService } from './search.service';
import * as fromSearch from '../reducers';
import * as formAuth from '../../auth/reducers';

describe('SearchService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
        StoreModule.forRoot({
          search: combineReducers(fromSearch.reducer),
          auth: combineReducers(formAuth.reducers),
        }),
      ],
      providers: [SearchService]
    });
  });
  it('should be created', inject([SearchService], (service: SearchService) => {
    expect(service).toBeTruthy();
  }));
});
