import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { SpotifyApiService } from '../../api/spotify-api.service';

import { SearchResponce } from '../models/search.model';

@Injectable()
export class SearchService {
  constructor(private api: SpotifyApiService) {}

  searchForItem(query: string): Observable<SearchResponce> {
    return this.api.searchForItem<SearchResponce>(query);
  }
}
