import { TestBed } from '@angular/core/testing';
import { Actions } from '@ngrx/effects';
import { cold, hot, getTestScheduler } from 'jasmine-marbles';
import { Observable } from 'rxjs/Observable';
import { empty } from 'rxjs/observable/empty';

import { SearchItem, SearchItemSuccess, SearchItemFail } from '../actions/search.actions';
import { SearchEffects, SEARCH_SCHEDULER, SEARCH_DEBOUNCE } from './search';
import { SearchService } from '../services/search.service';
import { SearchResponce } from '../models/search.model';

export class TestActions extends Actions {
  constructor() {
    super(empty());
  }

  set stream(source: Observable<any>) {
    this.source = source;
  }
}

export function getActions() {
  return new TestActions();
}

describe('SearchEffects', () => {
  let effects: SearchEffects;
  let searchService: any;
  let actions$: TestActions;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        SearchEffects,
        {
          provide: SearchService,
          useValue: {
            searchForItem: jest.fn(),
          },
        },
        { provide: Actions, useFactory: getActions },
        { provide: SEARCH_SCHEDULER, useFactory: getTestScheduler },
        { provide: SEARCH_DEBOUNCE, useValue: 10 },
      ],
    });

    effects = TestBed.get(SearchEffects);
    searchService = TestBed.get(SearchService);
    actions$ = TestBed.get(Actions);
  });

  describe('searchItem$', () => {
    it('should return a new SearchItemSuccess with search object, on success', () => {
      const query = 'test';
      const searchResponse = {
        artists: {
          items: [
            {
              id: '111',
              name: 'test',
            },
          ],
          total: 1,
        },
        tracks: {
          items: [
            {
              id: '111',
              name: 'test',
            },
          ],
          total: 1,
        },
      } as SearchResponce;
      const searchObj = {
        artists: [
          {
            id: '111',
            name: 'test',
          },
        ],
        tracks: [
          {
            id: '111',
            name: 'test',
          },
        ],
      };

      const action = new SearchItem(query);
      const completion = new SearchItemSuccess(searchObj);

      actions$.stream = hot('-a---', { a: action });
      const response = cold('-a|', { a: searchResponse });
      const expected = cold('---b', { b: completion });
      searchService.searchForItem = jest.fn(() => response);

      expect(effects.searchItem$).toBeObservable(expected);
    });

    it('should return a new SearchItemFail with error, on error', () => {
      const query = 'test';
      const error = 'Something wrong happened.';

      const action = new SearchItem(query);
      const completion = new SearchItemFail(error);

      actions$.stream = hot('-a---', { a: action });
      const response = cold('-#|', {}, error);
      const expected = cold('---b', { b: completion });
      searchService.searchForItem = jest.fn(() => response);

      expect(effects.searchItem$).toBeObservable(expected);
    });

    it('should not do anything if the query is an empty string', () => {
      const action = new SearchItem('');

      actions$.stream = hot('-a---', { a: action });
      const expected = cold('---');

      expect(effects.searchItem$).toBeObservable(expected);
    });
  });
});
