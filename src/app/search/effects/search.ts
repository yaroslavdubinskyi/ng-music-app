import { Injectable, InjectionToken, Optional, Inject } from '@angular/core';
import { Action } from '@ngrx/store';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { Observable } from 'rxjs/Observable';
import { defer } from 'rxjs/observable/defer';
import { of } from 'rxjs/observable/of';
import { Scheduler } from 'rxjs/Scheduler';
import { async } from 'rxjs/scheduler/async';
import { empty } from 'rxjs/observable/empty';
import { switchMap, toArray, map, catchError, mergeMap, debounceTime } from 'rxjs/operators';

import {
  SearchItem,
  SearchItemSuccess,
  SearchActionTypes,
  SearchItemFail,
} from '../actions/search.actions';
import { SearchService } from '../services/search.service';
import { SearchResponce } from '../models/search.model';

export const SEARCH_DEBOUNCE = new InjectionToken<number>('Search Debounce');
export const SEARCH_SCHEDULER = new InjectionToken<Scheduler>('Search Scheduler');

@Injectable()
export class SearchEffects {
  @Effect()
  searchItem$: Observable<Action> = this.actions$.pipe(
    ofType(SearchActionTypes.SearchItem),
    debounceTime(this.debounce || 500, this.scheduler || async),
    map((action: SearchItem) => action.payload),
    switchMap(query => {
      if (query === '') {
        return empty();
      }
      return this.searchService.searchForItem(query).pipe(
        map(
          (response: SearchResponce) =>
            new SearchItemSuccess({
              tracks: response.tracks.items,
              artists: response.artists.items,
            }),
        ),
        catchError(error => of(new SearchItemFail(error))),
      );
    }),
  );

  constructor(
    private actions$: Actions,
    private searchService: SearchService,
    @Optional()
    @Inject(SEARCH_DEBOUNCE)
    private debounce: number,
    @Optional()
    @Inject(SEARCH_SCHEDULER)
    private scheduler: Scheduler,
  ) {}
}
