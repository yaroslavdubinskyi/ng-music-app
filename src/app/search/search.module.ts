import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import { reducers } from './reducers';

import { SpotifyApiService } from '../api/spotify-api.service';
import { SearchService } from './services/search.service';

import { SearchEffects } from './effects/search';
import { SearchFormComponent } from './containers/search-form/search-form.component';
import { SearchInputComponent } from './components/search-input/search-input.component';
import { SearchTrackItemComponent } from './components/search-track-item/search-track-item.component';
import { SearchArtistItemComponent } from './components/search-artist-item/search-artist-item.component';

@NgModule({
  imports: [
    CommonModule,
    StoreModule.forFeature('search', reducers),
    EffectsModule.forFeature([SearchEffects]),
  ],
  declarations: [
    SearchFormComponent,
    SearchInputComponent,
    SearchTrackItemComponent,
    SearchArtistItemComponent,
  ],
  exports: [SearchFormComponent],
  providers: [SpotifyApiService, SearchService],
})
export class SearchModule {}
