import { Track } from '../../models/track.model';
import { ArtistFull } from '../../models/artist.model';

export interface SearchResponce {
  artists?: {
    items: Track[];
    total: number;
  };
  tracks?: {
    items: ArtistFull[];
    total: number;
  };
}
