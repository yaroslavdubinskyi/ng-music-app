import { reducer } from './search';
import * as fromSearch from './search';
import { SearchItem, SearchItemSuccess } from '../actions/search.actions';
import { Track } from '../../models/track.model';
import { ArtistFull } from '../../models/artist.model';

describe('SearchReducer', () => {
  describe('[undefined action]', () => {
    it('should return the default state', () => {
      const action = {} as any;
      const result = reducer(undefined, action);
      expect(result).toMatchSnapshot();
    });
  });

  describe('[SearchItem]', () => {
    it('should add search query and set loading to true, if query not empty', () => {
      const testQuery = 'test';
      const createAction = new SearchItem(testQuery);

      const expectedResult = {
        loading: true,
        query: testQuery,
      };

      const result = reducer(fromSearch.initialState, createAction);

      expect(result).toMatchSnapshot();
    });
    it('should set loaded and loading to false, if query is empty', () => {
      const testQuery = '';
      const createAction = new SearchItem(testQuery);

      const expectedResult = {
        loaded: false,
        loading: false,
        query: testQuery,
      };

      const result = reducer(fromSearch.initialState, createAction);

      expect(result).toMatchSnapshot();
    });
  });
  describe('[SearchItemSuccess]', () => {
    it('should add search results in search state', () => {
      const artist = { name: 'Test artist' } as ArtistFull;
      const track = { name: 'Test track' } as Track;
      const response = {
        tracks: [track],
        artists: [artist],
      };

      const createAction = new SearchItemSuccess(response);

      const expectedResult = {
        loaded: true,
        loading: false,
        results: {
          tracks: [...response.tracks],
          artists: [...response.artists],
        },
      };

      const result = reducer(fromSearch.initialState, createAction);

      expect(result).toMatchSnapshot();
    });
  });
});
