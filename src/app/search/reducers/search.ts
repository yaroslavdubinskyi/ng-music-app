import { SearchActions, SearchActionTypes } from '../actions/search.actions';

import { SearchResponce } from '../models/search.model';
import { Track } from '../../models/track.model';
import { ArtistFull } from '../../models/artist.model';

export interface State {
  loaded: boolean;
  loading: boolean;
  query: string;
  results: {
    artists: ArtistFull[];
    tracks: Track[];
  };
}

export const initialState: State = {
  loaded: false,
  loading: false,
  query: '',
  results: {
    artists: [],
    tracks: [],
  },
};

export function reducer(state = initialState, action: SearchActions): State {
  switch (action.type) {
    case SearchActionTypes.SearchItem: {
      const query = action.payload;
      if (query === '') {
        return {
          ...state,
          loaded: false,
          loading: false,
          query,
        };
      }
      return {
        ...state,
        loading: true,
        query,
      };
    }

    case SearchActionTypes.SearchItemSuccess: {
      return {
        ...state,
        loaded: true,
        loading: false,
        results: {
          tracks: action.payload.tracks,
          artists: action.payload.artists,
        },
      };
    }

    default: {
      return state;
    }
  }
}

export const getSearchQuery = (state: State) => state.query;
export const getSearchResults = (state: State) => state.results;
export const getSearchResultsTracks = (state: State) => state.results.tracks;
export const getSearchResultsArtists = (state: State) => state.results.artists;
export const getSearchLoaded = (state: State) => state.loaded;
