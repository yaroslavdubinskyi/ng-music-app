import { createSelector, createFeatureSelector, ActionReducerMap } from '@ngrx/store';

import * as fromRoot from '../../reducers';
import * as fromSearch from './search';

export interface SearchState {
  searchDetails: fromSearch.State;
}

export interface State extends fromRoot.State {
  search: SearchState;
}

export const reducers: ActionReducerMap<SearchState> = {
  searchDetails: fromSearch.reducer,
};

export const selectSearchState = createFeatureSelector<SearchState>('search');

export const selectSearchDetailsState = createSelector(
  selectSearchState,
  (state: SearchState) => state.searchDetails,
);

export const getSearchQuery = createSelector(selectSearchDetailsState, fromSearch.getSearchQuery);

export const getSearchResults = createSelector(
  selectSearchDetailsState,
  fromSearch.getSearchResults,
);

export const getSearchResultsTracks = createSelector(
  selectSearchDetailsState,
  fromSearch.getSearchResultsTracks,
);

export const getSearchResultsArtists = createSelector(
  selectSearchDetailsState,
  fromSearch.getSearchResultsArtists,
);

export const getSearchLoaded = createSelector(selectSearchDetailsState, fromSearch.getSearchLoaded);
