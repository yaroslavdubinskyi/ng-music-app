import { Action } from '@ngrx/store';

export enum SearchActionTypes {
  SearchItem = '[Search] Search Item',
  SearchItemSuccess = '[Search] Search Item Success',
  SearchItemFail = '[Search] Search Item Fail',
}

/**
 * Search Item Action
 */
export class SearchItem implements Action {
  readonly type = SearchActionTypes.SearchItem;

  constructor(public payload: string) {}
}

export class SearchItemSuccess implements Action {
  readonly type = SearchActionTypes.SearchItemSuccess;

  constructor(public payload: any) {}
}

export class SearchItemFail implements Action {
  readonly type = SearchActionTypes.SearchItemFail;

  constructor(public payload: any) {}
}

export type SearchActions = SearchItem | SearchItemSuccess | SearchItemFail;
