const getCategoriesListMock = {
  categories: {
    href: 'https://api.spotify.com/v1/browse/categories?offset=0&limit=50',
    items: [
      {
        href: 'https://api.spotify.com/v1/browse/categories/toplists',
        icons: [
          {
            height: 275,
            url:
              'https://t.scdn.co/media/derived/toplists_11160599e6a04ac5d6f2757f5511778f_0_0_275_275.jpg',
            width: 275,
          },
        ],
        id: 'toplists',
        name: 'Top Lists',
      },
      {
        href: 'https://api.spotify.com/v1/browse/categories/popculture',
        icons: [
          {
            height: 274,
            url:
              'https://t.scdn.co/media/derived/trending-274x274_7b238f7217985e79d3664f2734347b98_0_0_274_274.jpg',
            width: 274,
          },
        ],
        id: 'popculture',
        name: 'Trending',
      },
    ],
    limit: 50,
    next: null,
    offset: 0,
    previous: null,
    total: 34,
  },
};

export default getCategoriesListMock;
