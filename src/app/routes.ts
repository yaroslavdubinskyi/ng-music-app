import { Routes } from '@angular/router';

import { AuthGuard } from './auth/services/auth-guard.service';

import { ProfilePageComponent } from './auth/containers/profile-page/profile-page.component';
import { LoginPageComponent } from './auth/containers/login-page/login-page.component';
import { ContactFormPageComponent } from './core/containers/contact-form-page/contact-form-page.component';
import { NotFoundPageComponent } from './core/containers/not-found-page/not-found-page.component';

export const routes: Routes = [
  { path: '', redirectTo: '/categories', pathMatch: 'full' },
  {
    path: 'login',
    component: LoginPageComponent,
  },
  {
    path: 'profile',
    component: ProfilePageComponent,
  },
  {
    path: 'categories',
    loadChildren: './categories/categories.module#CategoriesModule',
  },
  {
    path: 'playlists',
    loadChildren: './playlists/playlists.module#PlaylistsModule',
    canActivate: [AuthGuard],
  },
  {
    path: 'contact',
    component: ContactFormPageComponent,
    canActivate: [AuthGuard],
  },
  { path: '**', component: NotFoundPageComponent },
];
