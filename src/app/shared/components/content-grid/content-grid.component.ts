import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-content-grid',
  templateUrl: './content-grid.component.html',
  styleUrls: ['./content-grid.component.scss'],
})
export class ContentGridComponent {
  @Input() cols: string;
}
