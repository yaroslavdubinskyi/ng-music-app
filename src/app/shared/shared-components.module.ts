import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ContentGridComponent } from './components/content-grid/content-grid.component';

@NgModule({
  declarations: [ContentGridComponent],
  exports: [ContentGridComponent],
})
export class SharedComponentsModule {}
