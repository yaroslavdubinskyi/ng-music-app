export interface ExternalURL {
  spotify: string;
}

export interface Image {
  url: string;
  height: number;
  width: number;
}

export interface PaddingObj {
  items: object[];
  limit: number;
  offset: number;
  total: number;
  previous: string;
  next: string;
}
