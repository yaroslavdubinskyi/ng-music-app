import { Image, ExternalURL } from './shared.model';

export interface Album {
  name: string;
  release_date: string;
  images: Image[];
  external_urls: ExternalURL;
}
