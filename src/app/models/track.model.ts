import { Image, ExternalURL } from './shared.model';
import { Album } from './album.model';
import { Artist } from './artist.model';

export interface Track {
  id: string;
  name: string;
  type: string;
  duration_ms: number;
  external_urls: ExternalURL;
  album: Album;
  artists: Artist[];
}

export interface PlaylistTrack {
  added_at: string;
  track: Track;
}
