import { Image, ExternalURL } from './shared.model';

export interface Artist {
  id: string;
  name: string;
  type: string;
  external_urls: ExternalURL;
}

export interface ArtistFull extends Artist {
  images: Image[];
  genres: string[];
}
