import { AuthActions, AuthActionTypes } from './../actions/auth.actions';
import Token from '../models/auth.model';
import User from '../models/user.model';

export interface State {
  loggedIn: boolean;
  token: Token | null;
  user: User | null;
}

export const initialState: State = {
  loggedIn: false,
  token: null,
  user: null,
};

export function reducer(state = initialState, action: AuthActions): State {
  switch (action.type) {
    case AuthActionTypes.Login: {
      return {
        ...state,
        token: action.payload.token,
      };
    }
    case AuthActionTypes.LoginSuccess: {
      return {
        ...state,
        loggedIn: true,
        user: action.payload.user,
      };
    }
    case AuthActionTypes.Logout: {
      return initialState;
    }

    default: {
      return state;
    }
  }
}

export const getLoggedIn = (state: State) => state.loggedIn;
export const getUser = (state: State) => state.user;
export const getToken = (state: State) => state.token;
