import { reducer } from './auth';
import * as fromAuth from './auth';
import { Login, LoginSuccess, Logout } from '../actions/auth.actions';
import Token from '../models/auth.model';
import User from '../models/user.model';
import { exec } from 'child_process';

describe('AuthReducer', () => {
  describe('[undefined action]', () => {
    it('should return the default state', () => {
      const action = {} as any;

      const result = reducer(undefined, action);

      expect(result).toMatchSnapshot();
    });
  });

  describe('[LoginSuccess]', () => {
    it('should add a user info and set loggedIn to true in auth state', () => {
      const user = { display_name: 'test' } as User;
      const createAction = new LoginSuccess({ user });

      const expectedResult = {
        loggedIn: true,
        user: { display_name: 'test' },
      };

      const result = reducer(fromAuth.initialState, createAction);

      expect(result).toMatchSnapshot();
    });
  });

  describe('[Logout]', () => {
    it('should logout a user', () => {
      const initialState = {
        loggedIn: true,
        user: { display_name: 'test' },
      } as fromAuth.State;
      const createAction = new Logout();

      const expectedResult = fromAuth.initialState;

      const result = reducer(initialState, createAction);

      expect(result).toMatchSnapshot();
    });
  });
});
