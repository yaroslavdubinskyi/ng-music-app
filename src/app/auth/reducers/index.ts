import { createSelector, createFeatureSelector, ActionReducerMap } from '@ngrx/store';

import * as fromRoot from '../../reducers';
import * as fromAuth from './auth';

export interface State extends fromRoot.State {
  auth: fromAuth.State;
}

export const reducers = fromAuth.reducer;

export const selectAuthState = createFeatureSelector<fromAuth.State>('auth');

export const getLoggedIn = createSelector(selectAuthState, fromAuth.getLoggedIn);

export const getUser = createSelector(selectAuthState, fromAuth.getUser);

export const getToken = createSelector(selectAuthState, fromAuth.getToken);
