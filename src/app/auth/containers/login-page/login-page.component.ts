import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';

import { AuthService } from '../../services/auth.service';

import * as Auth from '../../actions/auth.actions';
import * as fromAuth from '../../reducers';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
})
export class LoginPageComponent implements OnInit {
  constructor(private authService: AuthService, private store: Store<fromAuth.State>) {}

  ngOnInit() {
    if (window.opener !== null) {
      const hash = window.location.hash.substr(1);

      const res = hash.split('&').reduce(function(result, item) {
        const parts = item.split('=');
        result[parts[0]] = parts[1];
        return result;
      }, {});
      window.opener.postMessage({ type: 'auth_req', token: { ...res } }, window.opener.location);
      window.close();
    }

    window.addEventListener(
      'message',
      e => {
        if (e.data.type === 'auth_req') {
          this.store.dispatch(new Auth.Login(e.data));
        }
      },
      false,
    );
  }

  loginCLickHandler() {
    this.authService.getPermisions();
  }
}
