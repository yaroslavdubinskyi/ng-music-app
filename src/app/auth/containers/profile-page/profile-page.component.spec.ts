import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { StoreModule, Store, combineReducers } from '@ngrx/store';

import * as fromAuth from '../../reducers';

import { ProfilePageComponent } from './profile-page.component';
import { ProfileInfoComponent } from '../../components/profile-info/profile-info.component';

describe('ProfilePageComponent', () => {
  let component: ProfilePageComponent;
  let fixture: ComponentFixture<ProfilePageComponent>;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        imports: [
          RouterModule,
          StoreModule.forRoot({
            auth: combineReducers(fromAuth.reducers),
          }),
        ],
        declarations: [ProfilePageComponent, ProfileInfoComponent],
        schemas: [NO_ERRORS_SCHEMA],
      }).compileComponents();
    }),
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfilePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
