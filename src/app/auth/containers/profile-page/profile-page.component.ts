import { Component } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Store, select } from '@ngrx/store';

import User from '../../../auth/models/user.model';
import * as fromAuth from '../../../auth/reducers';

@Component({
  selector: 'app-profile-page',
  templateUrl: './profile-page.component.html',
  styleUrls: ['./profile-page.component.scss'],
})
export class ProfilePageComponent {
  userInfo$: Observable<User>;

  constructor(private store: Store<fromAuth.State>) {
    this.userInfo$ = this.store.pipe(select(fromAuth.getUser));
  }
}
