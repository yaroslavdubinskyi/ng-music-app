import { Component, Input } from '@angular/core';
import User from '../../../auth/models/user.model';

@Component({
  selector: 'app-profile-info',
  templateUrl: './profile-info.component.html',
  styleUrls: ['./profile-info.component.scss'],
})
export class ProfileInfoComponent {
  @Input() info: User;

  get name() {
    return this.info.display_name;
  }

  get email() {
    return this.info.email;
  }

  get followersTotal() {
    return this.info.followers.total;
  }
}
