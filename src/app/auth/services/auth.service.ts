import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { Store } from '@ngrx/store';

import { environment } from '../../../environments/environment';
import * as Auth from '../actions/auth.actions';
import * as fromAuth from '../reducers';

import User from '../models/user.model';
import Token from '../models/auth.model';

@Injectable()
export class AuthService {
  apiUrl: string;
  clientId: string;
  redirectUrl: string;
  token: Token;
  testToken: Token;

  constructor(private http: HttpClient, private store: Store<fromAuth.State>) {
    if (environment.envName === 'qa') {
      this.apiUrl = 'http://localhost:8000';
    } else {
      this.apiUrl = 'https://api.spotify.com';
    }
    this.clientId = 'efa59985283e4d9fbceb8d0fd233b6e8';
    this.redirectUrl = 'http://localhost:4200/login/';
    this.testToken = {
      access_token: 'test',
      expires_in: 3600,
      token_type: 'Bearer',
    };
  }

  getPermisions() {
    if (environment.envName === 'qa') {
      this.store.dispatch(new Auth.Login({ token: this.testToken }));
    } else {
      const authorizationTokenUrl = `https://accounts.spotify.com/authorize?response_type=token&client_id=${
        this.clientId
      }&redirect_uri=${encodeURIComponent(this.redirectUrl)}&scope=user-read-email`;

      const width = 450;
      const height = 730;
      const left = screen.width / 2 - width / 2;
      const top = screen.height / 2 - height / 2;
      const w = window.open(
        authorizationTokenUrl,
        'Spotify',
        'menubar=no,location=no,resizable=no,scrollbars=no,status=no, width=' +
          width +
          ', height=' +
          height +
          ', top=' +
          top +
          ', left=' +
          left,
      );
    }
  }

  getToken(token: Token): Observable<Token> {
    return of(token);
  }

  getCurrentUserProfile(token: Token): Observable<User> {
    return this.http.get<User>(`${this.apiUrl}/v1/me`, {
      headers: { Authorization: `Bearer ${token.access_token}` },
    });
  }
}
