import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { HttpClientModule } from '@angular/common/http';

import { AuthEffects } from './effects/auth.effects';
import { reducers } from './reducers';

import { AuthService } from './services/auth.service';
import { AuthGuard } from './services/auth-guard.service';
import { LoginPageComponent } from './containers/login-page/login-page.component';
import { ProfilePageComponent } from './containers/profile-page/profile-page.component';
import { ProfileInfoComponent } from './components/profile-info/profile-info.component';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    StoreModule.forFeature('auth', reducers),
    EffectsModule.forFeature([AuthEffects]),
  ],
  declarations: [LoginPageComponent, ProfilePageComponent, ProfileInfoComponent],
  providers: [AuthService, AuthGuard],
})
export class AuthModule {}
