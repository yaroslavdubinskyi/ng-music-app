import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Store, select } from '@ngrx/store';

import { environment } from '../../environments/environment';

import * as fromRoot from '../reducers';
import * as fromAuth from '../auth/reducers';

import Token from '../auth/models/auth.model';

@Injectable()
export class SpotifyApiService {
  apiUrl: string;
  clientId: string;
  token$: Observable<Token>;
  _token: Token;

  constructor(private http: HttpClient, private store: Store<fromRoot.State>) {
    if (environment.envName === 'qa') {
      this.apiUrl = 'http://localhost:8000';
    } else {
      this.apiUrl = 'https://api.spotify.com/v1';
    }

    this.clientId = 'efa59985283e4d9fbceb8d0fd233b6e8';
    this.token$ = this.store.pipe(select(fromAuth.getToken));
    this.token$.subscribe((token: Token) => {
      this._token = token;
    });
  }

  // Categories api calls
  getCategoriesList<T>(): Observable<T> {
    return this.http.get<T>(`${this.apiUrl}/browse/categories`, {
      headers: { Authorization: `Bearer ${this._token.access_token}` },
      params: { offset: '0', limit: '50' },
    });
  }

  getCategoryDetails<T>(id: string): Observable<T> {
    return this.http.get<T>(`${this.apiUrl}/browse/categories/${id}`, {
      headers: { Authorization: `Bearer ${this._token.access_token}` },
    });
  }

  getCategoryPlaylists<T>(id: string): Observable<T> {
    return this.http.get<T>(`${this.apiUrl}/browse/categories/${id}/playlists`, {
      headers: { Authorization: `Bearer ${this._token.access_token}` },
      params: { offset: '0', limit: '20' },
    });
  }

  // Playlists api calls
  getPlaylist<T>(userId: string, listId: string): Observable<T> {
    return this.http.get<T>(`${this.apiUrl}/users/${userId}/playlists/${listId}`, {
      headers: { Authorization: `Bearer ${this._token.access_token}` },
    });
  }

  // Search api calls
  searchForItem<T>(query: string): Observable<T> {
    return this.http.get<T>(`${this.apiUrl}/search`, {
      headers: { Authorization: `Bearer ${this._token.access_token}` },
      params: { q: query, type: 'track,artist', limit: '5' },
    });
  }
}
