import { SinglePlaylistComponent } from './containers/single-playlist/single-playlist.component';

import { AuthGuard } from '../auth/services/auth-guard.service';

export const routes = [
  {
    path: 'playlists',
    canActivate: [AuthGuard],
    children: [
      {
        path: ':user_id/:list_id',
        component: SinglePlaylistComponent,
      },
    ],
  },
];
