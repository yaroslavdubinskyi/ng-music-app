export interface Track {
  id: string;
  name: string;
  track_number: number;
  duration_ms: number;
  external_urls: {
    spotify: string;
  };
  album?: {
    name: string;
  };
  artists: {
    name: string;
  }[];
}

export interface PlaylistTrack {
  added_at: string;
  is_local: boolean;
  track: Track;
}
