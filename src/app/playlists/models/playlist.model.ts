import { PlaylistTrack } from './track.model';

export interface Playlist {
  id: string;
  name: string;
  description: string;
  external_urls: {
    spotify: string;
  };
  images: {
    url: string;
    height: number;
    width: number;
  }[];
  tracks: {
    items: PlaylistTrack[];
    total: number;
  };
  owner: {
    id: string;
    display_name: string;
  };
  followers: {
    total: number;
  };
}
