import { Component, OnInit, OnDestroy, ChangeDetectionStrategy } from '@angular/core';
import {
  trigger,
  style,
  transition,
  animate,
  keyframes,
  query,
  stagger,
} from '@angular/animations';
import { ActivatedRoute } from '@angular/router';
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';

import * as fromPlaylists from '../../reducers';
import * as playlists from '../../actions/playtlists.actions';

import { Playlist } from '../../models/playlist.model';
import { PlaylistTrack } from '../../models/track.model';

@Component({
  selector: 'app-single-playlist',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './single-playlist.component.html',
  animations: [
    trigger('gridAnimation', [
      transition('* => *', [
        query(':enter', style({ opacity: 0 }), { optional: true }),
        query(
          ':enter',
          stagger('100ms', [
            animate(
              '400ms ease-in',
              keyframes([
                style({ opacity: 0, transform: 'translateY(50px)', offset: 0 }),
                style({ opacity: 1, transform: 'translateY(0)', offset: 1.0 }),
              ]),
            ),
          ]),
          { optional: true },
        ),
      ]),
    ]),
  ],
})
export class SinglePlaylistComponent implements OnInit, OnDestroy {
  userId: string;
  listId: string;
  playlistDetails$: Observable<Playlist>;
  tracksShortList$: Observable<PlaylistTrack[]>;
  private paramsSub: any;

  constructor(private route: ActivatedRoute, private store: Store<fromPlaylists.State>) {
    this.playlistDetails$ = store.pipe(select(fromPlaylists.getPlaylistDetails));
    this.tracksShortList$ = store.pipe(select(fromPlaylists.getPlaylistTrackShortList));
  }

  ngOnInit() {
    this.paramsSub = this.route.params.subscribe(params => {
      window.scrollTo(0, 0);
      this.userId = params['user_id'];
      this.listId = params['list_id'];
      this.store.dispatch(new playlists.GetPlaylist({ userId: this.userId, listId: this.listId }));
    });
  }

  ngOnDestroy() {
    this.paramsSub.unsubscribe();
  }
}
