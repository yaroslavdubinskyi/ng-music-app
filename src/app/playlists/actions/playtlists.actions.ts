import { Action } from '@ngrx/store';

import { Playlist } from '../models/playlist.model';

export enum PlaylistActionTypes {
  GetPlaylist = '[Playlist] Get Playlist',
  GetPlaylistSuccess = '[Playlist] Get Playlist Success',
  GetPlaylistFail = '[Playlist] Get Playlist Fail',
  GetPlaylistTracks = '[Playlist] Get Playlist Tracks',
  GetPlaylistTracksSuccess = '[Playlist] Get Playlist Tracks Success',
  GetPlaylistTracksFail = '[Playlist] Get Playlist Tracks Fail',
}

/**
 * Get Playlist Action
 */
export class GetPlaylist implements Action {
  readonly type = PlaylistActionTypes.GetPlaylist;

  constructor(public payload: { userId: string; listId: string }) {}
}

export class GetPlaylistSuccess implements Action {
  readonly type = PlaylistActionTypes.GetPlaylistSuccess;

  constructor(public payload: Playlist) {}
}

export class GetPlaylistFail implements Action {
  readonly type = PlaylistActionTypes.GetPlaylistFail;

  constructor(public payload: any) {}
}

export type PlaylistActions = GetPlaylist | GetPlaylistSuccess | GetPlaylistFail;
