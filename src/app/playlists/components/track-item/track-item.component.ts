import { Component, Input } from '@angular/core';

import { PlaylistTrack } from '../../models/track.model';

@Component({
  selector: 'app-track-item',
  templateUrl: './track-item.component.html',
  styleUrls: ['./track-item.component.scss'],
})
export class TrackItemComponent {
  @Input() details: PlaylistTrack;

  get number() {
    return this.details.track.track_number;
  }

  get name() {
    return this.details.track.name;
  }

  get artists() {
    return this.details.track.artists.map(item => item.name).join(', ');
  }

  get album() {
    return this.details.track.album.name;
  }

  get trackURL() {
    return this.details.track.external_urls.spotify;
  }

}
