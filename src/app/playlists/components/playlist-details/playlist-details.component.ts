import { Component, Input } from '@angular/core';

import { Playlist } from '../../models/playlist.model';

@Component({
  selector: 'app-playlist-details',
  templateUrl: './playlist-details.component.html',
  styleUrls: ['./playlist-details.component.scss'],
})
export class PlaylistDetailsComponent {
  @Input() details: Playlist;

  get name() {
    return this.details.name;
  }

  get description() {
    return this.details.description;
  }

  get playlistURL() {
    return this.details.external_urls.spotify;
  }

  get imgSrc() {
    return this.details.images[0].url;
  }

  get tracksCount() {
    return this.details.tracks.total;
  }

  get followersCount() {
    return this.details.followers.total;
  }

  get ownersName() {
    return this.details.owner.display_name;
  }
}
