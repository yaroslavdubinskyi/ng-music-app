import { PlaylistActions, PlaylistActionTypes } from '../actions/playtlists.actions';

import { Playlist } from '../models/playlist.model';

export interface State {
  details: Playlist;
  loaded: boolean;
  loading: boolean;
}

export const initialState: State = {
  details: {
    id: '',
    name: '',
    external_urls: {
      spotify: '',
    },
    description: '',
    images: [
      {
        url: '',
        height: 0,
        width: 0,
      },
    ],
    tracks: {
      items: [],
      total: 0,
    },
    owner: {
      id: '',
      display_name: '',
    },
    followers: {
      total: 0,
    },
  },
  loaded: false,
  loading: false,
};

export function reducer(state = initialState, action: PlaylistActions): State {
  switch (action.type) {
    case PlaylistActionTypes.GetPlaylist: {
      return {
        ...state,
        loading: true,
      };
    }

    case PlaylistActionTypes.GetPlaylistSuccess: {
      return {
        ...state,
        loaded: true,
        loading: false,
        details: action.payload,
      };
    }

    default: {
      return state;
    }
  }
}

export const getPlaylistDetails = (state: State) => state.details;

export const getPlaylistTrackShortList = (state: State) => state.details.tracks.items;
