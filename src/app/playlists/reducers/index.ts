import { createSelector, createFeatureSelector, ActionReducerMap } from '@ngrx/store';

import * as fromRoot from '../../reducers';
import * as fromPlaylists from './playlists';

export interface PlaylistsState {
  playlistDetails: fromPlaylists.State;
}

export interface State extends fromRoot.State {
  playlists: PlaylistsState;
}

export const reducers: ActionReducerMap<PlaylistsState> = {
  playlistDetails: fromPlaylists.reducer,
};

export const selectPlaylistsState = createFeatureSelector<PlaylistsState>('playlists');

export const selectPlaylistInfoState = createSelector(
  selectPlaylistsState,
  (state: PlaylistsState) => state.playlistDetails,
);

export const getPlaylistDetails = createSelector(
  selectPlaylistInfoState,
  fromPlaylists.getPlaylistDetails,
);

export const getPlaylistTrackShortList = createSelector(
  selectPlaylistInfoState,
  fromPlaylists.getPlaylistTrackShortList,
);
