import { reducer } from './playlists';
import * as fromPlaylists from './playlists';
import { GetPlaylist, GetPlaylistSuccess } from '../actions/playtlists.actions';
import { Playlist } from '../models/playlist.model';

describe('PlaylistsReducer', () => {
  describe('[undefined action]', () => {
    it('should return the default state', () => {
      const action = {} as any;
      const result = reducer(undefined, action);
      expect(result).toMatchSnapshot();
    });
  });

  describe('[GetPlaylist]', () => {
    it('should set loading to true in playlist state', () => {
      const userId = 'test';
      const listId = 'test';
      const createAction = new GetPlaylist({ userId, listId });

      const exceptedResult = {
        loading: true,
      };

      const result = reducer(fromPlaylists.initialState, createAction);

      expect(result).toMatchSnapshot();
    });
  });

  describe('[GetPlaylistSuccess]', () => {
    it('should add playlists details, set loading to false and loaded to true in categories state', () => {
      const listDetails = {
        name: 'Test',
      } as Playlist;
      const createAction = new GetPlaylistSuccess(listDetails);

      const exceptedResult = {
        loaded: true,
        loading: false,
        details: listDetails,
      };

      const result = reducer(fromPlaylists.initialState, createAction);

      expect(result).toMatchSnapshot();
    });
  });
});
