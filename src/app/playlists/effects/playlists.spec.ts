import { TestBed } from '@angular/core/testing';
import { Actions } from '@ngrx/effects';
import { cold, hot } from 'jasmine-marbles';
import { Observable } from 'rxjs/Observable';
import { empty } from 'rxjs/observable/empty';

import { GetPlaylist, GetPlaylistSuccess, GetPlaylistFail } from '../actions/playtlists.actions';
import { PlaylistsEffects } from './playlists';
import { PlaylistsService } from '../services/playlists.service';
import { Playlist } from '../models/playlist.model';
import { PlaylistTrack, Track } from '../models/track.model';

export class TestActions extends Actions {
  constructor() {
    super(empty());
  }

  set stream(source: Observable<any>) {
    this.source = source;
  }
}

export function getActions() {
  return new TestActions();
}

describe('PlaylistsEffects', () => {
  let effects: PlaylistsEffects;
  let playlistsService: any;
  let actions$: TestActions;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        PlaylistsEffects,
        {
          provide: PlaylistsService,
          useValue: {
            getPlaylist: jest.fn(),
          },
        },
        { provide: Actions, useFactory: getActions },
      ],
    });

    effects = TestBed.get(PlaylistsEffects);
    playlistsService = TestBed.get(PlaylistsService);
    actions$ = TestBed.get(Actions);
  });

  describe('getCatDetails$', () => {
    it('should return a new GetPlaylistSuccess with playlist object, on success', () => {
      const userId = 'test';
      const listId = 'test';
      const playlist = { id: '111', name: 'Test Playlist' } as Playlist;

      const action = new GetPlaylist({ userId, listId });
      const completion = new GetPlaylistSuccess(playlist);

      actions$.stream = hot('-a---', { a: action });
      const responce = cold('-a|', { a: playlist });
      const expected = cold('--b', { b: completion });
      playlistsService.getPlaylist = jest.fn(() => responce);

      expect(effects.getCatDetails$).toBeObservable(expected);
    });
    it('should return a new GetPlaylistFail with error, on error', () => {
      const userId = '';
      const listId = '';
      const error = 'Something bad happened!';

      const action = new GetPlaylist({ userId, listId });
      const completion = new GetPlaylistFail(error);

      actions$.stream = hot('-a---', { a: action });
      const responce = cold('-#|', {}, error);
      const expected = cold('--b', { b: completion });
      playlistsService.getPlaylist = jest.fn(() => responce);

      expect(effects.getCatDetails$).toBeObservable(expected);
    });
  });
});
