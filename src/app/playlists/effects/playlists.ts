import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { Observable } from 'rxjs/Observable';
import { defer } from 'rxjs/observable/defer';
import { of } from 'rxjs/observable/of';
import { exhaustMap, toArray, map, catchError, mergeMap } from 'rxjs/operators';

import {
  GetPlaylist,
  GetPlaylistSuccess,
  GetPlaylistFail,
  PlaylistActionTypes,
} from '../actions/playtlists.actions';
import { PlaylistsService } from '../services/playlists.service';
import { Playlist } from '../models/playlist.model';

@Injectable()
export class PlaylistsEffects {
  @Effect()
  getCatDetails$: Observable<Action> = this.actions$.pipe(
    ofType(PlaylistActionTypes.GetPlaylist),
    map((action: GetPlaylist) => action.payload),
    exhaustMap(payload =>
      this.playlistsService
        .getPlaylist(payload.userId, payload.listId)
        .pipe(
          map((response: Playlist) => new GetPlaylistSuccess(response)),
          catchError(error => of(new GetPlaylistFail(error))),
        ),
    ),
  );

  // @Effect()
  // getPlaylists$: Observable<Action> = this.actions$.pipe(
  //   ofType(PlaylistActionTypes.GetPlaylistSuccess),
  //   map((action: GetCategoryPlaylists) => action.payload),
  //   exhaustMap(payload =>
  //     this.categoriesService
  //       .getCategoryPlaylists(payload)
  //       .pipe(
  //         map(
  //           (response: PlaylistsApi) => new GetCategoryPlaylistsSuccess(response.playlists.items),
  //         ),
  //         catchError(error => of(new GetCategoryPlaylistsFail(error))),
  //       ),
  //   ),
  // );

  constructor(private actions$: Actions, private playlistsService: PlaylistsService) {}
}
