import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { SpotifyApiService } from '../../api/spotify-api.service';

import { Playlist } from '../models/playlist.model';

@Injectable()
export class PlaylistsService {
  constructor(private api: SpotifyApiService) {}

  getPlaylist(userId: string, listId: string): Observable<Playlist> {
    return this.api.getPlaylist<Playlist>(userId, listId);
  }
}
