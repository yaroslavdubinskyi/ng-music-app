import { TestBed, inject } from '@angular/core/testing';
import { HttpClientModule } from '@angular/common/http';
import { StoreModule, Store, combineReducers } from '@ngrx/store';

import { PlaylistsService } from './playlists.service';
import * as fromPlaylists from '../reducers';
import * as fromAuth from '../../auth/reducers';

describe('PlaylistsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
        StoreModule.forRoot({
          categories: combineReducers(fromPlaylists.reducers),
          auth: combineReducers(fromAuth.reducers),
        }),
      ],
      providers: [PlaylistsService],
    });
  });

  it(
    'should be created',
    inject([PlaylistsService], (service: PlaylistsService) => {
      expect(service).toBeTruthy();
    }),
  );
});
