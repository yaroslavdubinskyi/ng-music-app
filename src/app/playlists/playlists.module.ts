import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import { SharedComponentsModule } from '../shared/shared-components.module';

import { PlaylistsEffects } from './effects/playlists';
import { reducers } from './reducers';

import { SpotifyApiService } from '../api/spotify-api.service';
import { PlaylistsService } from './services/playlists.service';

import { routes } from './playlists.routes';
import { ContentGridComponent } from '../shared/components/content-grid/content-grid.component';
import { SinglePlaylistComponent } from './containers/single-playlist/single-playlist.component';
import { PlaylistDetailsComponent } from './components/playlist-details/playlist-details.component';
import { TrackItemComponent } from './components/track-item/track-item.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    StoreModule.forFeature('playlists', reducers),
    EffectsModule.forFeature([PlaylistsEffects]),
    SharedComponentsModule,
  ],
  providers: [SpotifyApiService, PlaylistsService],
  declarations: [SinglePlaylistComponent, PlaylistDetailsComponent, TrackItemComponent],
})
export class PlaylistsModule {}
