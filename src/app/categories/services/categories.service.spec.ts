import { TestBed, inject } from '@angular/core/testing';
import { HttpClientModule } from '@angular/common/http';
import { StoreModule, Store, combineReducers } from '@ngrx/store';

import { CategoriesService } from './categories.service';
import * as formCategories from '../reducers';
import * as fromAuth from '../../auth/reducers';

describe('CategoriesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
        StoreModule.forRoot({
          categories: combineReducers(formCategories.reducers),
          auth: combineReducers(fromAuth.reducers),
        }),
      ],
      providers: [CategoriesService],
    });
  });

  it(
    'should be created',
    inject([CategoriesService], (service: CategoriesService) => {
      expect(service).toBeTruthy();
    }),
  );
});
