import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { SpotifyApiService } from '../../api/spotify-api.service';

import { Category, CategoriesApi } from '../models/category.model';
import { Playlist, PlaylistsApi } from '../models/playlist.model';

@Injectable()
export class CategoriesService {
  constructor(private api: SpotifyApiService) {}

  getCategoriesList(): Observable<CategoriesApi> {
    return this.api.getCategoriesList<CategoriesApi>();
  }

  getCategoryDetails(id: string): Observable<Category> {
    return this.api.getCategoryDetails<Category>(id);
  }

  getCategoryPlaylists(id: string): Observable<PlaylistsApi> {
    return this.api.getCategoryPlaylists<PlaylistsApi>(id);
  }
}
