import { Component, OnInit, OnDestroy, ChangeDetectionStrategy } from '@angular/core';
import {
  trigger,
  style,
  transition,
  animate,
  keyframes,
  query,
  stagger,
} from '@angular/animations';
import { ActivatedRoute } from '@angular/router';
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';

import * as fromCategories from '../../reducers';
import * as categories from '../../actions/categories.actions';

import { Category } from '../../models/category.model';
import { Playlist } from '../../models/playlist.model';

@Component({
  selector: 'app-category-page',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './category-page.component.html',
  animations: [
    trigger('gridAnimation', [
      transition('* => *', [
        query(':enter', style({ opacity: 0 }), { optional: true }),
        query(
          ':enter',
          stagger('100ms', [
            animate(
              '400ms ease-in',
              keyframes([
                style({ opacity: 0, transform: 'translateY(50px)', offset: 0 }),
                style({ opacity: 1, transform: 'translateY(0)', offset: 1.0 }),
              ]),
            ),
          ]),
          { optional: true },
        ),
      ]),
    ]),
  ],
})
export class CategoryPageComponent implements OnInit, OnDestroy {
  catId: string;
  categoryDetails$: Observable<Category>;
  playlists$: Observable<Playlist[]>;
  private _catId: any;

  constructor(private route: ActivatedRoute, private store: Store<fromCategories.State>) {
    this.categoryDetails$ = store.pipe(select(fromCategories.getCategoryDetails));
    this.playlists$ = store.pipe(select(fromCategories.getPlaylists));
  }

  ngOnInit() {
    this._catId = this.route.params.subscribe(params => {
      window.scrollTo(0, 0);
      this.catId = params['id'];
      this.store.dispatch(new categories.GetCategoryDetails(this.catId));
      this.store.dispatch(new categories.GetCategoryPlaylists(this.catId));
    });
  }

  ngOnDestroy() {
    this._catId.unsubscribe();
  }
}
