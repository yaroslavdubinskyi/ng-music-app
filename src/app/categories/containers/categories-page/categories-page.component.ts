import { Component, OnInit } from '@angular/core';
import {
  trigger,
  style,
  transition,
  animate,
  keyframes,
  query,
  stagger,
} from '@angular/animations';
import { Store, select } from '@ngrx/store';

import * as fromCategories from '../../reducers';
import * as categories from '../../actions/categories.actions';
import { Category } from '../../models/category.model';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-categories-page',
  templateUrl: './categories-page.component.html',
  animations: [
    trigger('gridAnimation', [
      transition('* => *', [
        query(':enter', style({ opacity: 0 }), { optional: true }),
        query(
          ':enter',
          stagger('50ms', [
            animate(
              '400ms ease-in',
              keyframes([
                style({ opacity: 0, transform: 'translateY(50px)', offset: 0 }),
                style({ opacity: 1, transform: 'translateY(0)', offset: 1.0 }),
              ]),
            ),
          ]),
          { optional: true },
        ),
      ]),
    ]),
  ],
})
export class CategoriesPageComponent implements OnInit {
  categories$: Observable<Category[]>;

  constructor(private store: Store<fromCategories.State>) {
    this.categories$ = store.pipe(select(fromCategories.getCategories));
  }

  ngOnInit() {
    this.store.dispatch(new categories.GetCategories());
  }
}
