import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { Observable } from 'rxjs/Observable';
import { defer } from 'rxjs/observable/defer';
import { of } from 'rxjs/observable/of';
import { exhaustMap, toArray, map, catchError, mergeMap } from 'rxjs/operators';

import {
  GetCategoryDetails,
  GetCategoryDetailsSuccess,
  GetCategoryDetailsFail,
  GetCategoryPlaylists,
  GetCategoryPlaylistsSuccess,
  GetCategoryPlaylistsFail,
  CategoryActionTypes,
} from '../actions/categories.actions';
import { CategoriesService } from '../services/categories.service';
import { Category } from '../models/category.model';
import { PlaylistsApi } from '../models/playlist.model';

@Injectable()
export class SingleCategoryEffects {
  @Effect()
  getCatDetails$: Observable<Action> = this.actions$.pipe(
    ofType(CategoryActionTypes.GetCategoryDetails),
    map((action: GetCategoryDetails) => action.payload),
    exhaustMap(payload =>
      this.categoriesService
        .getCategoryDetails(payload)
        .pipe(
          map(
            (response: Category) => new GetCategoryDetailsSuccess(response),
          ),
          catchError(error => of(new GetCategoryDetailsFail(error))),
        ),
    ),
  );

  @Effect()
  getPlaylists$: Observable<Action> = this.actions$.pipe(
    ofType(CategoryActionTypes.GetCategoryPlaylists),
    map((action: GetCategoryPlaylists) => action.payload),
    exhaustMap(payload =>
      this.categoriesService
        .getCategoryPlaylists(payload)
        .pipe(
          map(
            (response: PlaylistsApi) => new GetCategoryPlaylistsSuccess(response.playlists.items),
          ),
          catchError(error => of(new GetCategoryPlaylistsFail(error))),
        ),
    ),
  );

  constructor(private actions$: Actions, private categoriesService: CategoriesService) {}
}
