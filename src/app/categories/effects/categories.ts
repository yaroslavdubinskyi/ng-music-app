import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { Observable } from 'rxjs/Observable';
import { defer } from 'rxjs/observable/defer';
import { of } from 'rxjs/observable/of';
import { switchMap, toArray, map, catchError, mergeMap } from 'rxjs/operators';

import {
  GetCategories,
  GetCategoriesSuccess,
  GetCategoriesFail,
  CategoryActionTypes,
} from '../actions/categories.actions';
import { CategoriesService } from '../services/categories.service';
import { CategoriesApi } from '../models/category.model';

@Injectable()
export class CategoriesEffects {
  @Effect()
  getCategories$: Observable<Action> = this.actions$.pipe(
    ofType(CategoryActionTypes.GetCategories),
    switchMap(() =>
      this.categoriesService
        .getCategoriesList()
        .pipe(
          map((response: CategoriesApi) => new GetCategoriesSuccess(response.categories.items)),
          catchError(error => of(new GetCategoriesFail(error))),
        ),
    ),
  );

  constructor(private actions$: Actions, private categoriesService: CategoriesService) {}
}
