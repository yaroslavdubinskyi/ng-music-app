import { TestBed } from '@angular/core/testing';
import { Actions } from '@ngrx/effects';
import { cold, hot } from 'jasmine-marbles';
import { Observable } from 'rxjs/Observable';
import { empty } from 'rxjs/observable/empty';

import {
  GetCategories,
  GetCategoriesSuccess,
  GetCategoriesFail,
} from '../actions/categories.actions';
import { CategoriesEffects } from './categories';
import { CategoriesService } from '../services/categories.service';
import { CategoriesApi, Category } from '../models/category.model';

export class TestActions extends Actions {
  constructor() {
    super(empty());
  }

  set stream(source: Observable<any>) {
    this.source = source;
  }
}

export function getActions() {
  return new TestActions();
}

describe('CategoriesEffects', () => {
  let effects: CategoriesEffects;
  let categoriesService: any;
  let actions$: TestActions;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        CategoriesEffects,
        {
          provide: CategoriesService,
          useValue: { getCategoriesList: jest.fn() },
        },
        { provide: Actions, useFactory: getActions },
      ],
    });

    effects = TestBed.get(CategoriesEffects);
    categoriesService = TestBed.get(CategoriesService);
    actions$ = TestBed.get(Actions);
  });

  describe('getCategories$', () => {
    it('should return a new GetCategoriesSuccess with categories list, on success', () => {
      const cat1 = { id: '111', name: 'Category 1' } as Category;
      const cat2 = { id: '222', name: 'Category 2' } as Category;
      const cats = [cat1, cat2];
      const catsApiResponse = {
        categories: {
          items: [cat1, cat2],
        },
      };
      const action = new GetCategories();
      const completion = new GetCategoriesSuccess(cats);

      actions$.stream = hot('-a---', { a: action });
      const response = cold('-a|', { a: catsApiResponse });
      const expected = cold('--b', { b: completion });
      categoriesService.getCategoriesList = jest.fn(() => response);

      expect(effects.getCategories$).toBeObservable(expected);
    });
  });
});
