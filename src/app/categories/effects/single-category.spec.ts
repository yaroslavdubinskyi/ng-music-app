import { TestBed } from '@angular/core/testing';
import { Actions } from '@ngrx/effects';
import { cold, hot } from 'jasmine-marbles';
import { Observable } from 'rxjs/Observable';
import { empty } from 'rxjs/observable/empty';

import {
  GetCategoryDetails,
  GetCategoryDetailsSuccess,
  GetCategoryDetailsFail,
  GetCategoryPlaylists,
  GetCategoryPlaylistsSuccess,
  GetCategoryPlaylistsFail,
} from '../actions/categories.actions';
import { SingleCategoryEffects } from './single-category';
import { CategoriesService } from '../services/categories.service';
import { Category } from '../models/category.model';
import { PlaylistsApi, Playlist } from '../models/playlist.model';

export class TestActions extends Actions {
  constructor() {
    super(empty());
  }

  set stream(source: Observable<any>) {
    this.source = source;
  }
}

export function getActions() {
  return new TestActions();
}

describe('SingleCategoryEffects', () => {
  let effects: SingleCategoryEffects;
  let categoriesService: any;
  let actions$: TestActions;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        SingleCategoryEffects,
        {
          provide: CategoriesService,
          useValue: {
            getCategoryDetails: jest.fn(),
            getCategoryPlaylists: jest.fn(),
          },
        },
        { provide: Actions, useFactory: getActions },
      ],
    });

    effects = TestBed.get(SingleCategoryEffects);
    categoriesService = TestBed.get(CategoriesService);
    actions$ = TestBed.get(Actions);
  });

  describe('getCatDetails$', () => {

    it('should return a new GetCategoryDetailsSuccess with category details object, on success', () => {
      const catId = 'test';
      const category = { id: 'test', name: 'Test Category' } as Category;

      const action = new GetCategoryDetails(catId);
      const completion = new GetCategoryDetailsSuccess(category);

      actions$.stream = hot('-a---', { a: action });
      const response = cold('-a|', { a: category });
      const expected = cold('--b', { b: completion });
      categoriesService.getCategoryDetails = jest.fn(() => response);

      expect(effects.getCatDetails$).toBeObservable(expected);
    });

    it('should return a new GetCategoryDetailsFail, on error', () => {
      const catId = 'no';
      const error = 'Something wrong happed';

      const action = new GetCategoryDetails(catId);
      const completion = new GetCategoryDetailsFail(error);

      actions$.stream = hot('-a---', { a: action });
      const response = cold('-#|', {}, error);
      const expected = cold('--b', { b: completion });
      categoriesService.getCategoryDetails = jest.fn(() => response);

      expect(effects.getCatDetails$).toBeObservable(expected);
    });
  });

  describe('getPlaylists$', () => {
    it('should return a new GetCategoryPlaylistsSuccess with playlists, on success', () => {
      const catId = 'test';
      const playlist1 = { id: '111', name: 'Test Playlist 1' } as Playlist;
      const playlist2 = { id: '111', name: 'Test Playlist 2' } as Playlist;
      const playlists = [playlist1, playlist2];
      const playlistsResponse = {
        playlists: {
          items: [playlist1, playlist2],
        },
      } as PlaylistsApi;

      const action = new GetCategoryPlaylists(catId);
      const completion = new GetCategoryPlaylistsSuccess(playlists);

      actions$.stream = hot('-a---', { a: action });
      const response = cold('-a|', { a: playlistsResponse });
      const expected = cold('--b', { b: completion });
      categoriesService.getCategoryPlaylists = jest.fn(() => response);

      expect(effects.getPlaylists$).toBeObservable(expected);
    });

    it('should return a new GetCategoryPlaylistsFail, on error', () => {
      const catId = 'no';
      const error = 'Something wrong happed';

      const action = new GetCategoryPlaylists(catId);
      const completion = new GetCategoryPlaylistsFail(error);

      actions$.stream = hot('-a---', { a: action });
      const response = cold('-#|', {}, error);
      const expected = cold('--b', { b: completion });
      categoriesService.getCategoryPlaylists = jest.fn(() => response);

      expect(effects.getPlaylists$).toBeObservable(expected);
    });
  });
});
