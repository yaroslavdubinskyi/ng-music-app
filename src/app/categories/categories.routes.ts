import { CategoriesPageComponent } from './containers/categories-page/categories-page.component';
import { CategoryPageComponent } from './containers/category-page/category-page.component';

import { AuthGuard } from '../auth/services/auth-guard.service';

export const routes = [
  {
    path: 'categories',
    canActivate: [AuthGuard],
    children: [
      { path: '', component: CategoriesPageComponent },
      {
        path: ':id',
        component: CategoryPageComponent,
      },
    ],
  },
];
