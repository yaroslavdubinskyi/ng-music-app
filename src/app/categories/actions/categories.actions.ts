import { Action } from '@ngrx/store';

import { Category } from '../models/category.model';
import { Playlist } from '../models/playlist.model';

export enum CategoryActionTypes {
  GetCategories = '[Category] Get Categories',
  GetCategoriesSuccess = '[Category] Get Categories Success',
  GetCategoriesFail = '[Category] Get Categories Fail',
  GetCategoryDetails = '[Category] Get Category Details',
  GetCategoryDetailsSuccess = '[Category] Get Category Details Success',
  GetCategoryDetailsFail = '[Category] Get Category Details Fail',
  GetCategoryPlaylists = '[Category] Get Category Playlists',
  GetCategoryPlaylistsSuccess = '[Category] Get Category Playlists Success',
  GetCategoryPlaylistsFail = '[Category] Get Category Playlists Fail',
}

/**
 * Get Categories Action
 */
export class GetCategories implements Action {
  readonly type = CategoryActionTypes.GetCategories;
}

export class GetCategoriesSuccess implements Action {
  readonly type = CategoryActionTypes.GetCategoriesSuccess;

  constructor(public payload: Category[]) {}
}

export class GetCategoriesFail implements Action {
  readonly type = CategoryActionTypes.GetCategoriesFail;

  constructor(public payload: any) {}
}

/**
 * Get Category Details Action
 */
export class GetCategoryDetails implements Action {
  readonly type = CategoryActionTypes.GetCategoryDetails;

  constructor(public payload: string) {}
}

export class GetCategoryDetailsSuccess implements Action {
  readonly type = CategoryActionTypes.GetCategoryDetailsSuccess;

  constructor(public payload: Category) {}
}

export class GetCategoryDetailsFail implements Action {
  readonly type = CategoryActionTypes.GetCategoryDetailsFail;

  constructor(public payload: any) {}
}

/**
 * Get Category Playlists Action
 */
export class GetCategoryPlaylists implements Action {
  readonly type = CategoryActionTypes.GetCategoryPlaylists;

  constructor(public payload: string) {}
}

export class GetCategoryPlaylistsSuccess implements Action {
  readonly type = CategoryActionTypes.GetCategoryPlaylistsSuccess;

  constructor(public payload: Playlist[]) {}
}

export class GetCategoryPlaylistsFail implements Action {
  readonly type = CategoryActionTypes.GetCategoryPlaylistsFail;

  constructor(public payload: any) {}
}

export type CategoryActions =
  | GetCategories
  | GetCategoriesSuccess
  | GetCategoriesFail
  | GetCategoryDetails
  | GetCategoryDetailsSuccess
  | GetCategoryDetailsFail
  | GetCategoryPlaylists
  | GetCategoryPlaylistsSuccess
  | GetCategoryPlaylistsFail;
