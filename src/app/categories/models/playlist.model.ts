export interface Playlist {
  id: string;
  name: string;
  href: string;
  images: {
    url: string;
    height: number;
    width: number;
  };
  collaborative: boolean;
  tracks: {
    href: string;
    total: number;
  };
  owner: {
    id: string;
  };
}

export interface PlaylistsApi {
  playlists: {
    items: Playlist[];
  };
  next: string;
  previous: string;
}
