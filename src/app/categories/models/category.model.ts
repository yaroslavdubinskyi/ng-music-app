export interface Category {
  id: string;
  name: string;
  href: string;
  icons: {
    url: string;
    height: number;
    width: number;
  };
}

export interface CategoriesApi {
  categories: {
    items: Category[];
  };
}
