import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import { SharedComponentsModule } from '../shared/shared-components.module';

import { CategoriesEffects } from './effects/categories';
import { SingleCategoryEffects } from './effects/single-category';
import { reducers } from './reducers';

import { CategoriesPageComponent } from './containers/categories-page/categories-page.component';
import { SpotifyApiService } from '../api/spotify-api.service';
import { CategoriesService } from './services/categories.service';
import { CategoriesItemComponent } from './components/categories-item/categories-item.component';
import { CategoryPageComponent } from './containers/category-page/category-page.component';

import { routes } from './categories.routes';
import { PlaylistItemComponent } from './components/playlist-item/playlist-item.component';
import { CategoryDetailsComponent } from './components/category-details/category-details.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    StoreModule.forFeature('categories', reducers),
    EffectsModule.forFeature([CategoriesEffects, SingleCategoryEffects]),
    SharedComponentsModule,
  ],
  declarations: [
    CategoriesPageComponent,
    CategoriesItemComponent,
    CategoryPageComponent,
    PlaylistItemComponent,
    CategoryDetailsComponent,
  ],
  providers: [SpotifyApiService, CategoriesService],
})
export class CategoriesModule {}
