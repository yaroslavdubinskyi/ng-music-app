import { CategoryActions, CategoryActionTypes } from '../actions/categories.actions';

import { Category } from '../models/category.model';

export interface State {
  loaded: boolean;
  loading: boolean;
  items: Category[];
}

export const initialState: State = {
  loaded: false,
  loading: false,
  items: [],
};

export function reducer(state = initialState, action: CategoryActions): State {
  switch (action.type) {
    case CategoryActionTypes.GetCategories: {
      return {
        ...state,
        loading: true,
      };
    }

    case CategoryActionTypes.GetCategoriesSuccess: {
      return {
        ...state,
        loaded: true,
        loading: false,
        items: [...action.payload],
      };
    }

    default: {
      return state;
    }
  }
}

export const getCategories = (state: State) => state.items;
