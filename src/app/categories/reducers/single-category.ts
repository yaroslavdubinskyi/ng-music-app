import { CategoryActions, CategoryActionTypes } from '../actions/categories.actions';

import { Category } from '../models/category.model';
import { Playlist } from '../models/playlist.model';

export interface State {
  currentCatId: string;
  details: Category;
  playlists: {
    items: Playlist[];
  };
  loaded: boolean;
  loading: boolean;
}

export const initialState: State = {
  currentCatId: '',
  details: {
    id: '',
    name: '',
    href: '',
    icons: {
      url: '',
      height: 0,
      width: 0,
    },
  },
  playlists: {
    items: [],
  },
  loaded: false,
  loading: false,
};

export function reducer(state = initialState, action: CategoryActions): State {
  switch (action.type) {
    case CategoryActionTypes.GetCategoryDetails: {
      return {
        ...state,
        loading: true,
        currentCatId: action.payload,
      };
    }
    case CategoryActionTypes.GetCategoryDetailsSuccess: {
      return {
        ...state,
        loaded: true,
        loading: false,
        details: action.payload,
      };
    }
    case CategoryActionTypes.GetCategoryPlaylists: {
      return {
        ...state,
        loading: true,
        currentCatId: action.payload,
      };
    }

    case CategoryActionTypes.GetCategoryPlaylistsSuccess: {
      return {
        ...state,
        loaded: true,
        loading: false,
        playlists: {
          items: [...action.payload],
        },
      };
    }

    default: {
      return state;
    }
  }
}

export const getCategoryDetails = (state: State) => state.details;
export const getPlaylists = (state: State) => state.playlists.items;
