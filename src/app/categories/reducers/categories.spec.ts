import { reducer } from './categories';
import * as fromCategories from './categories';
import { GetCategories, GetCategoriesSuccess } from '../actions/categories.actions';
import { CategoriesApi, Category } from '../models/category.model';

describe('CategoriesReducer', () => {
  describe('[undefined action]', () => {
    it('should return the default state', () => {
      const action = {} as any;
      const result = reducer(undefined, action);
      expect(result).toMatchSnapshot();
    });
  });

  describe('[GetCategories]', () => {
    it('should set loading to true in categories state', () => {
      const createAction = new GetCategories();

      const exceptedResult = {
        loading: true,
      };

      const result = reducer(fromCategories.initialState, createAction);

      expect(result).toMatchSnapshot();
    });
  });

  describe('[GetCategoriesSuccess]', () => {
    it('should add items, set loading to false and loaded to true in categories state', () => {
      const cat1 = { id: '1', name: 'test cat 1' } as Category;
      const cat2 = { id: '2', name: 'test cat 2' } as Category;
      const cats = [cat1, cat2];
      const createAction = new GetCategoriesSuccess(cats);

      const exceptedResult = {
        loaded: true,
        loading: false,
        items: [...cats],
      };

      const result = reducer(fromCategories.initialState, createAction);

      expect(result).toMatchSnapshot();
    });
  });
});
