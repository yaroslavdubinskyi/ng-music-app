import { createSelector, createFeatureSelector, ActionReducerMap } from '@ngrx/store';

import * as fromRoot from '../../reducers';
import * as fromCategories from './categories';
import * as fromSingleCategory from './single-category';

export interface CategoriesState {
  categories: fromCategories.State;
  singleCategory: fromSingleCategory.State;
}

export interface State extends fromRoot.State {
  categories: CategoriesState;
}

export const reducers: ActionReducerMap<CategoriesState> = {
  categories: fromCategories.reducer,
  singleCategory: fromSingleCategory.reducer,
};

export const selectCategoriesState = createFeatureSelector<CategoriesState>('categories');

export const selectCategoriesListState = createSelector(
  selectCategoriesState,
  (state: CategoriesState) => state.categories,
);

export const getCategories = createSelector(
  selectCategoriesListState,
  fromCategories.getCategories,
);

export const selectCategoriesplaylistsState = createSelector(
  selectCategoriesState,
  (state: CategoriesState) => state.singleCategory,
);

export const getPlaylists = createSelector(
  selectCategoriesplaylistsState,
  fromSingleCategory.getPlaylists,
);

export const getCategoryDetails = createSelector(
  selectCategoriesplaylistsState,
  fromSingleCategory.getCategoryDetails,
);
