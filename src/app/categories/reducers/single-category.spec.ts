import { reducer } from './single-category';
import * as fromCategory from './single-category';
import {
  GetCategoryDetails,
  GetCategoryDetailsSuccess,
  GetCategoryPlaylists,
  GetCategoryPlaylistsSuccess,
} from '../actions/categories.actions';
import { Category } from '../models/category.model';
import { Playlist } from '../models/playlist.model';

describe('SingleCategoryReducer', () => {
  describe('[undefined action]', () => {
    it('should return the default state', () => {
      const action = {} as any;
      const result = reducer(undefined, action);
      expect(result).toMatchSnapshot();
    });
  });

  describe('[GetCategoryDetails]', () => {
    it('should set loading to true, add currentCatId in category state', () => {
      const catId = 'test';
      const createAction = new GetCategoryDetails(catId);

      const exceptedResult = {
        loading: true,
        currentCatId: catId,
      };

      const result = reducer(fromCategory.initialState, createAction);

      expect(result).toMatchSnapshot();
    });
  });

  describe('[GetCategoryDetailsSuccess]', () => {
    it('should add category details, set loading to false and loaded to true in categories state', () => {
      const catDetails = {
        name: 'Test',
      } as Category;
      const createAction = new GetCategoryDetailsSuccess(catDetails);

      const exceptedResult = {
        loaded: true,
        loading: false,
        details: catDetails,
      };

      const result = reducer(fromCategory.initialState, createAction);

      expect(result).toMatchSnapshot();
    });
  });

  describe('[GetCategoryPlaylists]', () => {
    it('should set loading to true, add currentCatId in category state', () => {
      const catId = 'test';
      const createAction = new GetCategoryPlaylists(catId);

      const exceptedResult = {
        loading: true,
        currentCatId: catId,
      };

      const result = reducer(fromCategory.initialState, createAction);

      expect(result).toMatchSnapshot();
    });
  });

  describe('[GetCategoryPlaylistsSuccess]', () => {
    it('should add category playlists, set loading to false and loaded to true in categories state', () => {
      const playlist1 = { name: 'Test 1' } as Playlist;
      const playlist2 = { name: 'Test 2' } as Playlist;
      const playlists = [playlist1, playlist2];
      const createAction = new GetCategoryPlaylistsSuccess(playlists);

      const exceptedResult = {
        loaded: true,
        loading: false,
        playlists: {
          items: [...playlists],
        },
      };

      const result = reducer(fromCategory.initialState, createAction);

      expect(result).toMatchSnapshot();
    });
  });
});
