import { Component, Input } from '@angular/core';

import { Playlist } from '../../models/playlist.model';

@Component({
  selector: 'app-playlist-item',
  templateUrl: './playlist-item.component.html',
  styleUrls: ['./playlist-item.component.scss'],
})
export class PlaylistItemComponent {
  @Input() playlist: Playlist;

  get listId() {
    return this.playlist.id;
  }

  get userId() {
    return this.playlist.owner.id;
  }

  get name() {
    return this.playlist.name;
  }

  get imgSrc() {
    return this.playlist.images[0].url;
  }
}
