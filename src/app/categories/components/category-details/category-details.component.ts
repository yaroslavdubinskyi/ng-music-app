import { Component, Input } from '@angular/core';

import { Category } from '../../models/category.model';

@Component({
  selector: 'app-category-details',
  templateUrl: './category-details.component.html',
  styleUrls: ['./category-details.component.scss']
})
export class CategoryDetailsComponent {
  @Input() details: Category;

  get name() {
    return this.details.name;
  }

}
