import { Component, Input } from '@angular/core';

import { Category } from '../../models/category.model';

@Component({
  selector: 'app-categories-item',
  templateUrl: './categories-item.component.html',
  styleUrls: ['./categories-item.component.scss'],
})
export class CategoriesItemComponent {
  @Input() details: Category;

  get catId() {
    return this.details.id;
  }

  get name() {
    return this.details.name;
  }

  get imgSrc() {
    return this.details.icons[0].url;
  }
}
