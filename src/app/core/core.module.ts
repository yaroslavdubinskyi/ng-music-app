import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './containers/app/app.component';
import { LayoutComponent } from './components/layout/layout.component';
import { HeaderComponent } from './components/header/header.component';
import { ContactFormPageComponent } from './containers/contact-form-page/contact-form-page.component';
import { NotFoundPageComponent } from './containers/not-found-page/not-found-page.component';

import { SearchModule } from '../search/search.module';

export const COMPONENTS = [AppComponent, LayoutComponent, HeaderComponent, ContactFormPageComponent, NotFoundPageComponent];

@NgModule({
  imports: [CommonModule, RouterModule, ReactiveFormsModule, SearchModule],
  declarations: COMPONENTS,
  exports: COMPONENTS,
})
export class CoreModule {}
