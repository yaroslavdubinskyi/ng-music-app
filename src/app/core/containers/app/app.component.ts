import { Observable } from 'rxjs/Observable';
import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';

import * as fromRoot from '../../../reducers';
import * as fromAuth from '../../../auth/reducers';
import * as Auth from '../../../auth/actions/auth.actions';

@Component({
  selector: 'app-music',
  templateUrl: './app.component.html',
})
export class AppComponent {
  loggedIn$: Observable<boolean>;

  constructor(private store: Store<fromRoot.State>) {
    this.loggedIn$ = this.store.pipe(select(fromAuth.getLoggedIn));
  }

  logout() {
    this.store.dispatch(new Auth.Logout());
  }
}
