import 'jest-preset-angular';
import './jestGlobalMocks';

Object.defineProperty(document.body.style, 'transform', {
  value: () => ({
    enumerable: true,
    configurable: true,
  }),
});
